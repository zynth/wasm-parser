meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_element_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: entries
      repeat: expr
      repeat-expr: count.value
      type: elem_segment
types:
  elem_segment: # https://webassembly.org/docs/modules/#exports
    seq:
        - id: index
          type: vlq_base128_le
        - id: offset
          terminator: 0x0b
        - id: num_elem
          type: vlq_base128_le
        - id: elems
          repeat: expr
          repeat-expr: num_elem.value
          type: vlq_base128_le