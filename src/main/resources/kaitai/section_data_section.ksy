meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_data_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: entries
      repeat: expr
      repeat-expr: count.value
      type: data_segment
types:
  data_segment: # https://webassembly.org/docs/modules/#exports
    seq:
        - id: index
          type: vlq_base128_le
        - id: offset
          terminator: 0x0b
        - id: size
          type: vlq_base128_le
        - id: data
          size: size.value