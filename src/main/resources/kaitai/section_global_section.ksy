meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_global_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: globals
      repeat: expr
      repeat-expr: count.value
      type: global_variable
types:
    global_type:
        seq:
            - id: content_type
              type: vlq_base128_le
        seq:
            - id: mutability
              type: u1
    global_variable:
        seq:
            - id: type
              type: global_type
            - id: offset
              terminator: 0x0b