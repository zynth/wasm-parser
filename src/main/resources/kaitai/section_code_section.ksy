meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_code_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: bodies
      repeat: expr
      repeat-expr: count.value
      type: function_body
types:
  local_entry:
    seq:
        - id: count
          type: vlq_base128_le
        - id: type
          type: vlq_base128_le
  function_body:
    seq:
        - id: body_size
          type: vlq_base128_le
        - id: local_count
          type: vlq_base128_le
        - id: locals
          repeat: expr
          repeat-expr: local_count.value
          type: local_entry
        - id: code
          terminator: 0x0b