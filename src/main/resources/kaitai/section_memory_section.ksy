meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_memory_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: entries
      repeat: expr
      repeat-expr: count.value
      type: memory_type
types:
    resizable_limits:
        seq:
            - id: flags
              type: u1
            - id: initial
              type: vlq_base128_le
            - id: maximum
              if: flags == 1
              type: vlq_base128_le
    memory_type:
        seq:
            - id: limits
              type: resizable_limits