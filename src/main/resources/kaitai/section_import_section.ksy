meta: # https://webassembly.org/docs/binary-encoding/#import-section
    id: section_import_section
    imports:
        - /common/vlq_base128_le
enums:
    external_kinds:
        0: function_kind
        1: table_kind
        2: memory_kind
        3: global_kind
seq:
    - id: import_count
      type: vlq_base128_le
    - id: import_entries
      repeat: expr
      repeat-expr: import_count.value
      type: import_entry
types:
    global_type:
        seq:
            - id: content_type
              type: vlq_base128_le
        seq:
            - id: mutability
              type: u1
    resizable_limits:
        seq:
            - id: flags
              type: u1
            - id: initial
              type: vlq_base128_le
            - id: maximum
              if: flags == 1
              type: vlq_base128_le
    memory_type:
        seq:
            - id: limits
              type: resizable_limits
    table_type:
        seq:
            - id: elem_type
              type: vlq_base128_le
            - id: limits
              type: resizable_limits
    import_entry:
        seq:
            - id: module_len
              type: vlq_base128_le
            - id: module_name_str
              type: str
              size: module_len.value
              encoding: ASCII
            - id: field_len
              type: vlq_base128_le
            - id: field_name_str
              type: str
              size: field_len.value
              encoding: ASCII
            - id: external_kind
              type: u1
              enum: external_kinds
            - id: function_kind
              if: external_kind == external_kinds::function_kind
              type: vlq_base128_le
            - id: table_kind
              if: external_kind == external_kinds::table_kind
              type: table_type
            - id: memory_kind
              if: external_kind == external_kinds::memory_kind
              type: memory_type
            - id: global_kind
              if: external_kind == external_kinds::global_kind
              type: global_type
          