meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_table_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: entries
      repeat: expr
      repeat-expr: count.value
      type: table_type
types:
    resizable_limits:
        seq:
            - id: flags
              type: u1
            - id: initial
              type: vlq_base128_le
            - id: maximum
              if: flags == 1
              type: vlq_base128_le
    table_type:
        seq:
            - id: elem_type
              type: vlq_base128_le
            - id: limits
              type: resizable_limits