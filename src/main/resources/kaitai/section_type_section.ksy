meta:
    id: section_type_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: function_count
      type: vlq_base128_le
    - id: function_types
      repeat: expr
      repeat-expr: function_count.value
      type: function_type
types:
  function_type: # https://webassembly.org/docs/binary-encoding/#func_type
    seq:
        - id: function_type_id
          type: vlq_base128_le
        - id: param_count
          type: vlq_base128_le
        - id: function_params
          repeat: expr
          repeat-expr: param_count.value
          type: vlq_base128_le
        - id: return_count
          type: vlq_base128_le
        - id: return_type
          if: return_count.value == 1
          type: vlq_base128_le