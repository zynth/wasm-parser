meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: section_function_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: count
      type: vlq_base128_le
    - id: types
      repeat: expr
      repeat-expr: count.value
      type: vlq_base128_le