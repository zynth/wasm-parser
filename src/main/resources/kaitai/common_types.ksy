meta: # https://webassembly.org/docs/binary-encoding/#function-section
    id: common_types
    imports:
        - /common/vlq_base128_le
enums:
    external_kinds:
        0: function_kind
        1: table_kind
        2: memory_kind
        3: global_kind
types:
    resizable_limits:
        seq:
            - id: flags
              type: u1
            - id: initial
              type: vlq_base128_le
            - id: maximum
              if: flags == 1
              type: vlq_base128_le
    memory_type:
        seq:
            - id: limits
              type: resizable_limits
    table_type:
        seq:
            - id: elem_type
              type: vlq_base128_le
            - id: limits
              type: resizable_limits