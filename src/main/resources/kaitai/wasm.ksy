meta:
  id: wasm
  file-extension: wasm
  endian: le
  imports:
    - /common/vlq_base128_le
    - section_custom_section
    - section_type_section
    - section_import_section
    - section_function_section
    - section_table_section
    - section_memory_section
    - section_global_section
    - section_export_section
    - section_start_section
    - section_element_section
    - section_code_section
    - section_data_section
seq:
    - id: header
      type: header
    - id: module
      type: module
types:
  header:
    seq:
      - id: magic
        contents: 
            - 0x00
            - 0x61
            - 0x73
            - 0x6D
        # expects \0asm
      - id: version
        type: u4
        # wasm version, currently always 1
  module:
    seq:
      - id: sections
        repeat: eos
        type: section
  section:
    seq:
      - id: section_id
        type: u1
      - id: section_count
        type: vlq_base128_le
      - id: section_body
        size: section_count.value
        type:
            switch-on: section_id
            cases:
                0: section_custom_section
                1: section_type_section # done
                2: section_import_section # done
                3: section_function_section # done
                4: section_table_section # done
                5: section_memory_section # done
                6: section_global_section # done
                7: section_export_section # done 
                8: section_start_section # done
                9: section_element_section # done
                10: section_code_section # done
                11: section_data_section # done
      