meta: # https://webassembly.org/docs/binary-encoding/#export-section
    id: section_export_section
    imports:
        - /common/vlq_base128_le
seq:
    - id: export_count
      type: vlq_base128_le
    - id: export_entries
      repeat: expr
      repeat-expr: export_count.value
      type: export_entry
types:
  export_entry: # https://webassembly.org/docs/modules/#exports
    seq:
        - id: field_len
          type: vlq_base128_le
        - id: field_name_str
          type: str
          size: field_len.value
          encoding: ASCII
        - id: kind
          type: vlq_base128_le
        - id: index
          type: vlq_base128_le