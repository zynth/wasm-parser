package io.zynth.chiba.parser;

import java.io.IOException;

import io.zynth.chiba.parser.SectionCodeSection.FunctionBody;
import io.zynth.chiba.parser.SectionExportSection.ExportEntry;
import io.zynth.chiba.parser.SectionImportSection.ImportEntry;
import io.zynth.chiba.parser.SectionTypeSection.FunctionType;
import io.zynth.chiba.parser.Wasm.Header;
import io.zynth.chiba.parser.Wasm.Section;

public class ParserTest {

	public static void main(String[] args) {
		long took = 0;
		long longest = 0;
		long shortest = 1000;
		for(int i=0; i<50000; i++) {
		try {
			long start = System.currentTimeMillis();
			Wasm wasm = Wasm.fromFile("wasm/test.wasm");
			Header header = wasm.header();
			byte[] magic = header.magic();
			long version = header.version();
			
			System.out.println("Version: " + version);
			
			for(Section section : wasm.module().sections()) {
				Object sectionBody = section.sectionBody();

				System.out.println(section.sectionId());
				System.out.println(section.sectionCount().value());
				
				if(sectionBody instanceof SectionTypeSection) {
					functionSection((SectionTypeSection) sectionBody);
				} else if(sectionBody instanceof SectionImportSection) {
					functionSection((SectionImportSection) sectionBody);
				} else if(sectionBody instanceof SectionCodeSection) {
					functionSection((SectionCodeSection) sectionBody);
				} else if(sectionBody instanceof SectionFunctionSection) {
					SectionFunctionSection functionSection = (SectionFunctionSection) sectionBody;
					System.out.println("Got function: " + functionSection);
					
				} else if(sectionBody instanceof SectionExportSection) {
					SectionExportSection exportSection = (SectionExportSection) sectionBody;
					System.out.println("Got export: " + exportSection);
					System.out.println("Export entries: " + exportSection.exportCount().value());
					for(ExportEntry entry : exportSection.exportEntries()) {
						String fieldNameStr = entry.fieldNameStr();
						System.out.println("Field Name: " + fieldNameStr);
					}
					
				} else if(sectionBody instanceof SectionCodeSection) {
					SectionCodeSection codeSection = (SectionCodeSection) sectionBody;
					System.out.println("Got code: " + codeSection);
					
				}
			}
			took = (System.currentTimeMillis()-start);
			longest = Math.max(longest, took);
			shortest = Math.min(shortest, took);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		System.out.println("Took: " + took + " long " +longest + " short " + shortest);
	}

	/**
	 * 
	 * @param importSection
	 */
	private static void functionSection(SectionCodeSection importSection) {
		for(FunctionBody body : importSection.bodies()) {
			System.out.println("Locals count: " + body.localCount().value());
			System.out.println("Body Size: " + body.bodySize().value());
			System.out.println("Body array size: " + body.code().length);
		}
	}
	
	/**
	 * 
	 * @param importSection
	 */
	private static void functionSection(SectionImportSection importSection) {
		System.out.println("Got type:" + importSection);
		System.out.println("Import count: " + importSection.importEntries());
		
		for(ImportEntry entry : importSection.importEntries()) {
			String fieldNameStr = entry.fieldNameStr();
			System.out.println("Import: " + fieldNameStr);
			System.out.println("Kind: " + entry.externalKind());
			
			switch(entry.externalKind()) {
				case FUNCTION_KIND:
					System.out.println("Function Kind Idx: " + entry.functionKind().value());
					break;
				case GLOBAL_KIND:
					break;
				case MEMORY_KIND:
					break;
				case TABLE_KIND:
					System.out.println("Table Kind: " + entry.tableKind().limits().flags());
					break;
				default:
					break;
			}
		}
	}
	
	/**
	 * 
	 * @param typeSection
	 */
	private static void functionSection(SectionTypeSection typeSection) {
		System.out.println("Got type:" + typeSection);
		System.out.println("Function count: " + typeSection.functionCount());
		System.out.println(typeSection.functionCount());
		
		for(FunctionType funcType : typeSection.functionTypes()) {
			System.out.println("param count: " + funcType.paramCount().value());
			
			for(VlqBase128Le param : funcType.functionParams()) {
				System.out.println("-- Parameter: " + param.value());
			}
			
			Integer returnCount = funcType.returnCount().value();
			System.out.println("return count: " + returnCount);
			if(returnCount == 1) {
				System.out.println("return type: " + funcType.returnType().value());
			}
		}
	}

}
